__author__ = 'crow'

from socket import socket, AF_INET, SOCK_STREAM

s = socket(AF_INET,SOCK_STREAM)

s.connect(('127.0.0.1', 8888))

sent_size = s.send(b'test')
received_data = s.recv(4096)

if received_data.decode('utf-8') == 'callback':
    print('ok: sended {} bytes'.format(sent_size))
else:
    print('fail')

s.close()