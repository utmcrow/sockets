__author__ = 'crow'

from socket import socket, AF_INET, SOCK_STREAM

s = socket(AF_INET,SOCK_STREAM)

s.bind(('127.0.0.1', 8888))
s.listen(10)

client, addr = s.accept()

received_data = client.recv(4096)

if received_data.decode('utf-8') == 'test':
    print('ok: received')
    client.send(b'callback')
else:
    print('fail')
    client.send(b'fail')

s.close()